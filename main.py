import logging
import time
import os
from concurrent.futures.thread import ThreadPoolExecutor

import requests
from pymongo import MongoClient, UpdateOne

logging.basicConfig()
logging.root.setLevel(logging.NOTSET)
BASE_URL = "http://api.pathofexile.com/public-stash-tabs"
client = MongoClient(os.environ['MONGO_DB_CLIENT_URL'])

def main():
    next_request_id = ''
    with ThreadPoolExecutor(max_workers=3) as executor:
        while True:
            try:
                logging.info("Starting request " + next_request_id)
                stash_information = requests.get(BASE_URL + '/' + next_request_id)
                if stash_information.status_code != 200:
                    time.sleep(30)
                    stash_information = requests.get(BASE_URL + '/' + next_request_id)
                stash_json = stash_information.json()
                executor.submit(load_into_db, stash_json)
                logging.info("Exectuor returned: ")
                if next_request_id == stash_json.get('next_change_id', ''):
                    logging.info("Waiting 30 seconds. Backing off.")
                    time.sleep(30)
                    continue
                next_request_id = stash_json.get('next_change_id', '')
                time.sleep(5)
            except Exception as e:
                logging.error("Failure in thread pool executor", e)


def load_into_db(stash_json):
    try:
        db = client['poe']
        updates = [UpdateOne({'id': stash['id']}, {'$set': stash}, upsert=True) for stash in stash_json.get('stashes', [])]
        db.player_stahes.bulk_write(updates)
        logging.info("Completed loading into db")
        return True
    except Exception as e:
        logging.error("Failure while loading into db: ", e)
        return False


def back_off():
    time.sleep(30)


if __name__ == '__main__':
    main()
