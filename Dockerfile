FROM python:3.8-slim

COPY requirements.txt /app/requirements.txt
COPY main.py /app/main.py

RUN pip install -r /app/requirements.txt

ENTRYPOINT python /app/main.py